import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StartPageComponent} from "./start-page/start-page.component";
import {LayoutComponent} from "./shared/layout/layout.component";

const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      {path: '', component: StartPageComponent},
      {
        path: 'single-responsibility',
        loadChildren: () => import('./single-responsibility/single-responsibility.module').then(m => m.SingleResponsibilityModule)
      },
      {
        path: 'open-close',
        loadChildren: () => import('./open-close/open-close.module').then(m => m.OpenCloseModule)
      },
      {
        path: 'liskov-substitution',
        loadChildren: () => import('./liskov-subs/liskov-subs.module').then(m => m.LiskovSubsModule)
      },
      {
        path: 'interface-segregation',
        loadChildren: () => import('./interface-segregation/interface-segregation.module').then(m => m.InterfaceSegregationModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
