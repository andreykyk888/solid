import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SingleResponPatternComponent} from "./single-respon-pattern/single-respon-pattern.component";
import {SingleResponAntipatternComponent} from "./single-respon-antipattern/single-respon-antipattern.component";
import {SharedModule} from "../shared/shared.module";
import {SingleResponMainComponent} from './single-respon-main/single-respon-main.component';
import {SingleResponsibilityRoutingModule} from "./single-responsibility-routing.module";
import {AppMatModule} from "../shared/app-mat.module";
import {SingleResponsibilityContentComponent} from './single-respon-pattern/single-responsibility-content/single-responsibility-content.component';
import {SingleResponPatternService} from "../shared/api/single-respon-pattern.service";
import { TitleSrpBlockComponent } from './single-respon-main/title-srp-block/title-srp-block.component';
import { AbstractSrpBlockComponent } from './single-respon-main/abstract-srp-block/abstract-srp-block.component';
import { ExampleSrpBlockComponent } from './single-respon-main/example-srp-block/example-srp-block.component';
import { AntiSrpBlockComponent } from './single-respon-main/anti-srp-block/anti-srp-block.component';

@NgModule({
  declarations: [
    SingleResponPatternComponent,
    SingleResponsibilityContentComponent,
    TitleSrpBlockComponent,
    AbstractSrpBlockComponent,
    ExampleSrpBlockComponent,
    AntiSrpBlockComponent,
    SingleResponMainComponent,
    SingleResponAntipatternComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SingleResponsibilityRoutingModule,
    AppMatModule
  ],
  providers: [SingleResponPatternService]
})
export class SingleResponsibilityModule {
}
