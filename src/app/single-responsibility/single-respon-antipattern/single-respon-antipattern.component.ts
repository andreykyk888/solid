import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {IMessage} from "../../shared/interfaces";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-single-respon-antipattern',
  templateUrl: './single-respon-antipattern.component.html',
  styleUrls: ['./single-respon-antipattern.component.scss']
})
export class SingleResponAntipatternComponent implements OnInit {
  isBlackTheme: boolean = false
  usersMessages: IMessage[] = []
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.getAllUsersMessages().subscribe(messages => {
      this.usersMessages = messages
    })
  }

  counterMessages() {
    return this.usersMessages.reduce((acc, message) => acc + message.id, 0)
  }

  getAllUsersMessages(): Observable<IMessage[]> {
    return this.http.get<IMessage[]>('http://jsonplaceholder.typicode.com/posts')
  }

}
