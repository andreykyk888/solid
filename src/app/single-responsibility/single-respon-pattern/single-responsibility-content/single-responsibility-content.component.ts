import { Component, OnInit } from '@angular/core';
import {SingleResponPatternService} from "../../../shared/api/single-respon-pattern.service";
import {IMessage} from "../../../shared/interfaces";

@Component({
  selector: 'app-single-responsibility-content',
  templateUrl: './single-responsibility-content.component.html',
  styleUrls: ['./single-responsibility-content.component.scss']
})
export class SingleResponsibilityContentComponent implements OnInit {
  usersMessages: IMessage[] = []

  constructor(private singleResponPatternService : SingleResponPatternService) { }

  ngOnInit(): void {
    this.singleResponPatternService.getAllUsersMessages().subscribe(messages => {
      this.usersMessages = messages
    })
  }

  counterMessages() {
    return this.usersMessages.reduce((acc, message) => acc + message.id, 0)
  }
}
