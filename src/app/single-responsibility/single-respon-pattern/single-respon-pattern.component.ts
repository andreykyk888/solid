import {Component} from '@angular/core';

@Component({
  selector: 'app-single-respon-pattern',
  templateUrl: './single-respon-pattern.component.html',
  styleUrls: ['./single-respon-pattern.component.scss']
})
export class SingleResponPatternComponent {
  isBlackTheme: boolean = false
}
