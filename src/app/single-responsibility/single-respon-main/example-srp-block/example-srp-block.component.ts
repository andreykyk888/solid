import { Component, OnInit } from '@angular/core';
import {SingleResponPatternComponent} from "../../single-respon-pattern/single-respon-pattern.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-example-srp-block',
  templateUrl: './example-srp-block.component.html',
  styleUrls: ['./example-srp-block.component.scss']
})
export class ExampleSrpBlockComponent {

  constructor(public dialog: MatDialog) { }

  openDialog() {
    this.dialog.open(SingleResponPatternComponent)
  }
}
