import {Component} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {SingleResponAntipatternComponent} from "../../single-respon-antipattern/single-respon-antipattern.component";

@Component({
  selector: 'app-anti-srp-block',
  templateUrl: './anti-srp-block.component.html',
  styleUrls: ['./anti-srp-block.component.scss']
})
export class AntiSrpBlockComponent {

  constructor(public dialog: MatDialog) { }

  openDialog() {
    this.dialog.open(SingleResponAntipatternComponent)
  }
}
