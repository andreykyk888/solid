import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SingleResponMainComponent} from "./single-respon-main/single-respon-main.component";

const routes: Routes = [
  {path: '', component: SingleResponMainComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class SingleResponsibilityRoutingModule {
}
