import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {LiskovSubsMainComponent} from "./liskov-subs-main/liskov-subs-main.component";

const routes: Routes = [
  {path: '', component: LiskovSubsMainComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LiskovSubsRoutingModule { }
