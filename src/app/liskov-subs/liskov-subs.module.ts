import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {AntiLsBlockComponent} from './liskov-subs-main/anti-ls-block/anti-ls-block.component';
import {AbstractLsBlockComponent} from './liskov-subs-main/abstract-ls-block/abstract-ls-block.component';
import {LiskovSubsMainComponent} from "./liskov-subs-main/liskov-subs-main.component";
import {LiskovSubsRoutingModule} from "./liskov-subs-routing.module";
import {ExampleLsBlockComponent} from './liskov-subs-main/example-ls-block/example-ls-block.component';
import {TitleLsBlockComponent} from './liskov-subs-main/title-ls-block/title-ls-block.component';

@NgModule({
  declarations: [
    AntiLsBlockComponent,
    AbstractLsBlockComponent,
    LiskovSubsMainComponent,
    ExampleLsBlockComponent,
    TitleLsBlockComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    LiskovSubsRoutingModule
  ]
})
export class LiskovSubsModule {
}
