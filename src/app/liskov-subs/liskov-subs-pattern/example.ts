import {IAdmin, IUser} from "../../shared/interfaces";

export class Person {
    firstName: string
    lastName: string
    role: string

    constructor(person: any) {
        this.firstName = person.name
        this.lastName = person.surname
        this.role = person.role
    }
}

export class Administration extends Person {
    constructor(person: IAdmin) {
        super(person)
    }
}

export class Admin extends Administration {
    canBan: boolean = true

    constructor(person: IAdmin) {
        super(person);
    }
}

export class Moderator extends Administration {
    canModerate: boolean = true

    constructor(person: IAdmin) {
        super(person);
    }
}

export class Users extends Person {
    canBeBanned:boolean = true
    rating: number

    constructor(person: IUser) {
        super(person);
        this.rating = person.rating
    }

    public getRating() {
        return this.rating
    }
}

export class User extends Users {
    canBuy: boolean = true

    constructor(person: IUser) {
        super(person);
    }
}

export class Salesman extends Users {
    canSell: boolean = true

    constructor(person: IUser) {
        super(person);
    }
}

const admin = {
    name: 'Петр',
    surname: 'Васильевич',
    role: 'admin',
}

const user = {
    name: 'Вася',
    surname: 'Пупкин',
    role: 'user',
    rating: 7
}

const moderator = {
    name: 'Иван',
    surname: 'Иванов',
    role: 'moderator',
}

const salesman = {
    name: 'Павел',
    surname: 'Поплавский',
    role: 'salesman',
    rating: 7
}

function logRating<T extends Users>(obj: T) {
    console.log(obj.getRating())
}

logRating(new User(user))
