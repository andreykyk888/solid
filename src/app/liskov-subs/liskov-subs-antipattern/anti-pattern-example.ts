import {IAdmin, IUser} from "../../shared/interfaces";

class Person {
    firstName: string
    lastName: string
    role: string
    rating: number

    constructor(person: any) {
        this.firstName = person.name
        this.lastName = person.surname
        this.role = person.role
        this.rating = person.rating
    }

    getRating():any {
        return this.rating
    }
}


class Admin extends Person {
    canBan: boolean = true

    constructor(person: IAdmin) {
        super(person);
    }
    getRating() {
        throw new Error('Я админ! У меня нету рейтинга!')
    }
}

class Moderator extends Person {
    canModerate: boolean = true

    constructor(person: IAdmin) {
        super(person);
    }
    getRating() {
        throw new Error('Я модератор! У меня нету рейтинга!')
    }
}

class User extends Person {
    canBuy: boolean = true
    canBeBanned: boolean = true

    constructor(person: IUser) {
        super(person);
    }
}

class Salesman extends Person {
    canSell: boolean = true
    canBeBanned: boolean = true

    constructor(person: IUser) {
        super(person);
    }
}

const admin = {
    name: 'Петр',
    surname: 'Васильевич',
    role: 'admin',
}

const user = {
    name: 'Вася',
    surname: 'Пупкин',
    role: 'user',
    rating: 7
}

const moderator = {
    name: 'Иван',
    surname: 'Иванов',
    role: 'moderator',
}

const salesman = {
    name: 'Павел',
    surname: 'Поплавский',
    role: 'salesman',
    rating: 7
}

function logRating<T extends Person>(obj: T) {
  if(obj !instanceof User) return;
  console.log(obj.getRating())
}

logRating(new Admin(admin))
