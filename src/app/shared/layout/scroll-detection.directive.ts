import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appScrollDetection]'
})
export class ScrollDetectionDirective {

  constructor(private element: ElementRef, private render: Renderer2) { }

  @HostListener('window:scroll')
  onScroll() {
    if(window.pageYOffset > 10) {
      this.render.setStyle(this.element.nativeElement, 'box-shadow', '0 5px 10px #737373FF')
      return
    }
    this.render.setStyle(this.element.nativeElement, 'box-shadow', null)
  }
}
