export interface IMessage {
  id: number;
}

export  interface ICreatedControl {
  controlName: string;
  label: string;
  width: string;
}

export interface IShowingControls {
  name: string;
  value: string;
}

export interface IEnvironments {
  production: boolean;
  apiMessages: string;
}

export interface IUser {
  name: string;
  surname: string;
  role: string;
  rating: number
}

export interface IAdmin {
  name: string;
  surname: string;
  role: string;
}
