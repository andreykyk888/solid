import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IMessage} from "../interfaces";
import {environment} from "../../../environments/environment";

@Injectable()
export class SingleResponPatternService {

  constructor(private http: HttpClient) {
  }

  getAllUsersMessages(): Observable<IMessage[]> {
    return this.http.get<IMessage[]>(environment.apiMessages)
  }
}
