import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from "@angular/common/http";
import {MainLayoutComponent} from "./layout/main-layout/main-layout.component";
import {AppComponent} from "../app.component";
import {StartPageComponent} from "../start-page/start-page.component";
import {LayoutComponent} from "./layout/layout.component";
import {LayoutMenuComponent} from "./layout/layout-menu/layout-menu.component";
import {ScrollDetectionDirective} from "./layout/scroll-detection.directive";
import {RouterModule} from "@angular/router";
import {AppMatModule} from "./app-mat.module";

const declarations = [
  MainLayoutComponent,
  StartPageComponent,
  LayoutComponent,
  LayoutMenuComponent,
  ScrollDetectionDirective,
]

const modules = [
  AppMatModule,
  HttpClientModule,
]

@NgModule({
  declarations: declarations,
  imports: [
    CommonModule,
    RouterModule,
    ...modules
  ],
  exports: [...declarations, ...modules]
})
export class SharedModule { }
