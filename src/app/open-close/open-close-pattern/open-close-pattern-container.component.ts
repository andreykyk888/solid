import {
  Component,
  OnInit
} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {ICreatedControl} from "../../shared/interfaces";
import {OcPatternService} from "./oc-pattern.service";

@Component({
  selector: 'app-open-close-pattern-container',
  templateUrl: './open-close-pattern-container.component.html',
  styleUrls: ['./open-close-pattern-container.component.scss'],
  viewProviders: [OcPatternService]
})
export class OpenClosePatternContainerComponent implements OnInit {
  form: FormGroup
  addControl$: Subject<ICreatedControl> = new Subject<ICreatedControl>()

  ngOnInit(): void {
    this.form = new FormGroup({
      controlName: new FormControl('', Validators.required),
      label: new FormControl('', Validators.required),
      width: new FormControl('', Validators.required),
    })
  }

  submit() {
    if (this.form.invalid) return;
    this.addControl$.next({
      ...this.form.value
    })
    this.form.reset()
  }

}
