import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {AbstractControl} from "@angular/forms";

@Injectable()
export class OcPatternService {
  controls$: Subject<{ [p: string]: AbstractControl }> = new Subject<{ [p: string]: AbstractControl }>()

  emitFormChanges(controls: { [p: string]: AbstractControl }) {
    this.controls$.next(controls)
  }
}
