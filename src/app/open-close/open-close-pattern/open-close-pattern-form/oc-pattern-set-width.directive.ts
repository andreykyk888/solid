import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appSetWidth]'
})
export class OcPatternSetWidthDirective implements OnInit {
  @Input() width: string;

  constructor(
    private element: ElementRef,
    private render: Renderer2
  ) {
  }

  ngOnInit(): void {
    this.render.setStyle(this.element.nativeElement, 'width', `${this.width}px`)
  }
}
