import {Component, Host, Input, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import {ICreatedControl} from "../../../shared/interfaces";
import {OcPatternService} from "../oc-pattern.service";

@Component({
  selector: 'app-open-close-pattern-form-builder',
  templateUrl: './oc-pattern-form-builder.component.html',
  styleUrls: ['./oc-pattern-form-builder.component.scss']
})
export class OcPatternFormBuilderComponent implements OnInit, OnDestroy {
  @Input() addControl$: Observable<ICreatedControl> = new Observable<ICreatedControl>()
  controlInfo: ICreatedControl[] = []
  form: FormGroup
  onChange: Function
  changeValue$: Subscription

  constructor(@Host() private ocPatternService: OcPatternService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({})
    this.addControl$.subscribe(control => {
      this.form.setControl(control.controlName, new FormControl('', Validators.required))
      this.controlInfo.push(control)
    })
    this.changeValue$ = this.form.valueChanges.subscribe(() => {
      this.ocPatternService.emitFormChanges(this.form.controls)
    })
  }

  ngOnDestroy(): void {
    if(!this.changeValue$) return
    this.changeValue$.unsubscribe()
  }
}
