import {Component, Host, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl} from "@angular/forms";
import {BehaviorSubject, Observable, Subject, Subscription} from "rxjs";
import {IShowingControls} from "../../../shared/interfaces";
import {OcPatternService} from "../oc-pattern.service";

@Component({
  selector: 'app-oc-pattern-show-controls',
  templateUrl: './oc-pattern-show-controls.component.html',
  styleUrls: ['./oc-pattern-show-controls.component.scss']
})
export class OcPatternShowControlsComponent implements OnInit, OnDestroy {
  showingControlsInfo: BehaviorSubject<IShowingControls[]> = new BehaviorSubject<IShowingControls[]>([])
  getControls$: Subscription

  constructor(@Host() private ocPatternService: OcPatternService) {
  }

  ngOnInit(): void {
    this.getControls$ = this.ocPatternService.controls$.subscribe(controls => {
      if(!controls) return;
      const currentControls:IShowingControls[] = []
      for(const control of Object.entries(controls)){
        currentControls.push({
          name: control[0],
          value: control[1].value
        })
      }
      this.showingControlsInfo.next(currentControls)
    })
  }

  ngOnDestroy() {
    if(!this.getControls$) return
    this.getControls$.unsubscribe()
  }

}
