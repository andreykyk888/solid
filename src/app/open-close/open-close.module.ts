import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OpenClosePatternContainerComponent} from "./open-close-pattern/open-close-pattern-container.component";
import {OpenCloseMainComponent} from './open-close-main/open-close-main.component';
import {OpenCloseRoutingModule} from "./open-close-routing.module";
import {AbstractOcBlockComponent} from './open-close-main/abstract-oc-block/abstract-oc-block.component';
import {AntiOcBlockComponent} from './open-close-main/anti-oc-block/anti-oc-block.component';
import {ExampleOcBlockComponent} from './open-close-main/example-oc-block/example-oc-block.component';
import {TitleOcBlockComponent} from './open-close-main/title-oc-block/title-oc-block.component';
import {SharedModule} from "../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";
import { OcPatternSetWidthDirective } from './open-close-pattern/open-close-pattern-form/oc-pattern-set-width.directive';
import { OcPatternShowControlsComponent } from './open-close-pattern/show-controls/oc-pattern-show-controls.component';
import {OcPatternFormBuilderComponent} from "./open-close-pattern/open-close-pattern-form/oc-pattern-form-builder.component";
import { OpenCloseAntipatternComponent } from './open-close-antipattern/open-close-antipattern.component';


@NgModule({
  declarations: [
    OpenClosePatternContainerComponent,
    OpenCloseMainComponent,
    AbstractOcBlockComponent,
    AntiOcBlockComponent,
    ExampleOcBlockComponent,
    TitleOcBlockComponent,
    OcPatternFormBuilderComponent,
    OcPatternSetWidthDirective,
    OcPatternShowControlsComponent,
    OpenCloseAntipatternComponent
  ],
  imports: [
    CommonModule,
    OpenCloseRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class OpenCloseModule {
}
