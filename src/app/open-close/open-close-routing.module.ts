import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {OpenCloseMainComponent} from "./open-close-main/open-close-main.component";

const routes: Routes = [
  {path: '', component: OpenCloseMainComponent}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OpenCloseRoutingModule {
}
