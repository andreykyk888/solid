import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-open-close-antipattern',
  templateUrl: './open-close-antipattern.component.html',
  styleUrls: ['./open-close-antipattern.component.scss']
})
export class OpenCloseAntipatternComponent implements OnInit {

  form: FormGroup

  constructor(
    public router: Router
  ) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      password: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required)
    })
    if(this.router.url === '/open-close') {
      this.form.addControl('phoneNumber', new FormControl('', Validators.required))
    }
    if(this.router.url === '/some-page') {
      this.form.removeControl('email')
      this.form.addControl('someControl', new FormControl('', Validators.required))
    }
  }

}
