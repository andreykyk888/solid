import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InterfaceSegrPatternComponent} from "./interface-segr-pattern/interface-segr-pattern.component";
import {InterfaceSegrAntipatternComponent} from "./interface-segr-antipattern/interface-segr-antipattern.component";
import {InterfaceSegrMainComponent} from "./interface-segr-main/interface-segr-main.component";
import {AbstractIsBlockComponent} from "./interface-segr-main/abstract-is-block/abstract-is-block.component";
import { TitleIsBlockComponent } from './interface-segr-main/title-is-block/title-is-block.component';
import { ExampleIsBlockComponent } from './interface-segr-main/example-is-block/example-is-block.component';
import { AntiIsBlockComponent } from './interface-segr-main/anti-is-block/anti-is-block.component';
import {SharedModule} from "../shared/shared.module";
import {InterfaceSegregationRoutingModule} from "./interface-segregation-routing.module";



@NgModule({
  declarations: [
    InterfaceSegrPatternComponent,
    InterfaceSegrAntipatternComponent,
    InterfaceSegrMainComponent,
    AbstractIsBlockComponent,
    TitleIsBlockComponent,
    ExampleIsBlockComponent,
    AntiIsBlockComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    InterfaceSegregationRoutingModule
  ]
})
export class InterfaceSegregationModule {

}
