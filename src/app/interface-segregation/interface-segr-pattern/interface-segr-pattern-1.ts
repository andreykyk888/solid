interface IPerson {
  firstName: string;
  lastName: string;
  middleName: string;
}

interface IDriver extends IPerson{
  drivingExp: string;
  drivingExpAge: number;
}

interface IBuilder extends IPerson {
  constructionExp: string;
  constructionExpAge: number;
}

class Person implements IPerson {
  firstName: 'Андрей';
  lastName: 'Чуприна';
  middleName: 'Павлович';
}

class Driver implements IDriver {
  drivingExp: 'Я работал в такси 3 года, было супер!';
  drivingExpAge: 3;
  firstName: 'Василий';
  lastName: 'Пупкин';
  middleName: 'Васильевич';
}

class Builder implements IBuilder {
  constructionExp: 'Я работал на стойке 3 года, построил дом!';
  constructionExpAge: 3;
  firstName: 'Петр';
  lastName: 'Петров';
  middleName: 'Петрович';
}
