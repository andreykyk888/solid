import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {InterfaceSegrMainComponent} from "./interface-segr-main/interface-segr-main.component";

const routes: Routes = [
  {path: '', component: InterfaceSegrMainComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class InterfaceSegregationRoutingModule { }
