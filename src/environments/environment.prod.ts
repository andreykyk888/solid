import {IEnvironments} from "../app/shared/interfaces";

export const environment: IEnvironments = {
  production: true,
  apiMessages: 'http://jsonplaceholder.typicode.com/posts'
};
